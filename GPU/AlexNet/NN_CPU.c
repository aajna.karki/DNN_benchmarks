#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<string.h>

#define L1_KERNEL_SIZE 11*11*3
#define L1_OUT 96
#define L2_KERNEL_SIZE 5*5*48
#define L2_OUT 256 
#define L3_KERNEL_SIZE 3*3*256
#define L3_OUT 384 
#define L4_KERNEL_SIZE 3*3*192
#define L4_OUT 384
#define L5_KERNEL_SIZE 3*3*192
#define L5_OUT 256
#define INPUT_SIZE 227*227*3

#define L1_FMAP 55*55
#define L2_FMAP 27*27
#define L3_FMAP 13*13
#define L4_FMAP 13*13
#define L5_FMAP 13*13
#define POOL1_FMAP 27*27
#define POOL2_FMAP 13*13
#define POOL3_FMAP 6*6

#define bool int 
#define true 1
#define false 0

void executeFirstLayer(float *bias,float *Layer1_Neurons_GPU,float *Layer1_Weights_GPU,float *Layer2_Neurons_GPU,int stride_width,int col_width,int feature_r,int feature_c,int out)
{
	float product = 0.0;
	int stride = 0,colstride = 0,output = 0,row = 0,col = 0,i = 0,j = 0;
	{
		for(output =0;output < out ;output++)
		{
			for(row =0; row < feature_r ;row++)
			{  
                                colstride = 3*row*stride_width*col_width;	
				stride = 0;
				for(col =0; col < feature_c ;col++)
				{
					product = 0;
					/* RGB weights and input 11*11*3 , kernel is 11*11 */
					for(i = 0; i < 11; i++)
					{			
						for(j = 0; j < 11; j++)
						{
							product +=        ((Layer1_Neurons_GPU[i*col_width*3 + j*3 + stride + colstride]    * Layer1_Weights_GPU[i*11 + j + (output * 11*11*3)])  
									+ (Layer1_Neurons_GPU[i*col_width*3 + j*3 + 1 + stride + colstride] * Layer1_Weights_GPU[i*11 + 11*11 + j+ (output * 11*11*3)])
									+ (Layer1_Neurons_GPU[i*col_width*3 + j*3 + 2 + stride + colstride] * Layer1_Weights_GPU[i*11 + 11*11*2 + j+ (output * 11*11*3)]));
						}
					}
					product += bias[output];	
                                        if(product < 0) /* RELU Layer */
						product = 0; // max(0,x)
					Layer2_Neurons_GPU[output*feature_r*feature_c + row*feature_c + col] = product;
#ifdef LAYER1_DEBUG			
					printf("%f\n",product);
#endif		
					product = 0.0;
					stride+= stride_width*3;	
				}
			}
		}
	}
}

void execute3Dconvolution(float *bias,float *Layer2_Neurons_GPU, float *Layer2_Weights_GPU,float *Layer3_Neurons_GPU,int out,int fr,int fc,int stride_width,int kernel,int pad,int in_output,int group)
{
	float product = 0.0;
        int x_pad = 0, y_pad = 0, loopc = 0,loopr = 0;
        printf(" 3D convolution with group %d,output %d,feature %d x %d ,stride %d, kernel %d, pad %d, input %d\n",group,out,fr,fc,stride_width,kernel,pad,in_output); 
	if(group == 2)
	{
		out = out >> 1;
		in_output = in_output >> 1;
	}
	int stride = 0,colstride = 0,output = 0,row = 0,col = 0,feature = 0,i =0,j =0;;
	{
		for(output =0;output < out  ;output++) /* out = 256 */
		{      
                        colstride = 0;	
			for(row =0; row < fr ; row++) /* out = 256 */
			{	
				stride = 0;	
				if(row > pad)
					colstride = (row - pad) * fr;
				for(col =0; col < fc ;col++) /* out = 256 */
				{
					x_pad = 0; y_pad = 0;
					/* set the loops value */
					loopc = kernel;loopr = kernel;
					/* take care of padding in left hand side of image*/ 
					if( row < pad)
					{
						x_pad = pad - row;
						loopr = kernel - x_pad;
					} 
					/* take care of padding in upper side of image*/ 
					if( col < pad )
					{
						y_pad = pad - col;
						loopc = kernel - y_pad;
					} 
					/* take care of padding in right side of image*/ 
					if(col >= fc - pad)
						loopc =  fc + pad - col;  
					/* take care of padding in bottom of image */ 
					if(row >= fr - pad)
						loopr =  fr + pad - row;
					for(feature =0; feature < in_output ; feature++) // calculate the feature maps
					{
						for(i =0; i < loopr ; i++) // kernel convolution
						{
							for(j =0; j < loopc ; j++) // kernel convolution
							{
								product += ( Layer2_Neurons_GPU[feature*fr*fc + i*fc + j + stride + colstride] * Layer2_Weights_GPU[output*kernel*kernel*in_output + feature*kernel*kernel + i*kernel + j + kernel*x_pad + y_pad]);
							}
						}
					}
                                        product += bias[output];
					if(product < 0) /* ReLU Layer */
						product = 0;
#ifdef LAYER2_DEBUG
					printf("%f\n",product);
#endif                  
//					if((group == 2) && (out == 128) && (in_output == 192))
//						printf("%f\n",product);
					Layer3_Neurons_GPU[output*fr*fc + row*fc + col] = product;
					product = 0.0;
					if(col >= pad)
						stride+=stride_width;
				}
			}

		}
		if(group == 2)
		{
			/* Execute second set of inputs */
			for(output = out ;output < (out << 1)   ;output++) /* out = 256 */
			{      
				colstride = 0;	
				for(row =0; row < fr; row++) /* out = 256 */
				{	
					stride = 0;	
					if(row > pad)
						colstride = (row - pad) * fr;
					for(col =0; col < fc ;col++) /* out = 256 */
					{
						x_pad = 0; y_pad = 0;
						/* set the loops value */
						loopc = kernel;loopr = kernel;
						/* take care of padding in left hand side of image*/ 
						if( row < pad)
						{
							x_pad = pad - row;
							loopr = kernel - x_pad;
						} 
						/* take care of padding in upper side of image*/ 
						if( col < pad )
						{
							y_pad = pad - col;
							loopc = kernel - y_pad;
						} 
						/* take care of padding in right side of image*/ 
						if(col >= fc - pad)
							loopc =  fc + pad - col;  
						/* take care of padding in bottom of image */ 
						if(row >= fr - pad)
							loopr =  fr + pad - row;
						for(feature = in_output ; feature < (in_output << 1) ; feature++) // calculate the feature maps
						{
							for(i =0; i < loopr ; i++) // kernel convolution
							{
								for(j =0; j < loopc ; j++) // kernel convolution
								{
									product += (( Layer2_Neurons_GPU[feature*fr*fc + i*fc + j + stride + colstride] * Layer2_Weights_GPU[output*kernel*kernel*in_output + (feature-in_output)*kernel*kernel + i*kernel + j + kernel*x_pad + y_pad]));
								}
							}
						}
						product += bias[output];
						if(product < 0) /* ReLU Layer */
							product = 0;
#ifdef LAYER2_DEBUG
						printf("%f\n",product);
#endif                   
//						if((group == 2) && (out == 128) && (in_output == 192))
//							printf("%f\n",product);
						Layer3_Neurons_GPU[output*fr*fc + row*fc + col] = product;
						product = 0.0;
						if(col >= pad)
							stride+=stride_width;
					}
				}

			}
		}

	}
}
void pooling(float *Layer2_Neurons_GPU,float *Layer2_pool_GPU,int out,int out_fr,int out_fc,int kernel,int stride_width,int in_fr,int in_fc)
{
        printf("pooling Activation layer \n");
	float max = 0.0;
	int downsample = 0;
	int stride = 0,colstride = 0,output = 0,row = 0,col = 0, i =0,j=0;
	{
		for(output =0;output < out ;output++)
		{
			for(row =0; row < out_fr ;row++)
			{  
                                colstride = row * stride_width*in_fc;	
				stride = 0;
				for(col =0; col < out_fc ;col++)
				{
					for(i = 0; i < kernel; i++)
					{			
						for(j = 0; j < kernel; j++)
						{
							if(max < ((Layer2_Neurons_GPU[(output*in_fr*in_fc) + i*in_fc + j + stride + colstride])))
								max =   ((Layer2_Neurons_GPU[(output*in_fr*in_fc) + i*in_fc + j + stride + colstride])) ;
				//			if(output == 141)
				//				printf("%f %d\t",Layer2_Neurons_GPU[(output*in_fr*in_fc) + i*in_fc + j + stride + colstride],((output*in_fr*in_fc) + i*in_fc + j + stride + colstride)) ;

						}
					}
					Layer2_pool_GPU[downsample] = max;
#ifdef POOL_DEBUG			
					printf("\n %f %d\n",max,downsample);
#endif	
					max = 0.0;
					downsample++;
					stride+= stride_width;	
				}
			}
		}
	}
}
void executelrnNorm(float *Layer_InNeurons_GPU, float alpha, float beta,int local_size,int out,int fr,int fc,float *Layer_OutNeurons_GPU)
{
        printf(" Exexcute Norm Layer\n");
        int nStart = 0, nEnd = 0,row =0,col = 0,output = 0,i =0;
        float value = 0.0;float sum = 0.0;
	for(row =0; row < fr; row++) 
	{	
		for(col =0; col < fc ;col++)
		{
			for(output = 0 ;output < out   ;output++)
			{
                                nStart=(output-floor(local_size/2)) > 1 ? (output-floor(local_size/2)) : 1 ;
                                nEnd=(output+floor(local_size/2)) <  out ? (output+floor(local_size/2)) : out ;
				for(i = (nStart-1); i < (nEnd-1) ; i++) // kernel convolution
				{
					sum += pow(( Layer_InNeurons_GPU[i*fr*fc + row*fc + col]),2);
				}
				value = (Layer_InNeurons_GPU[output*fr*fc + row*fc + col]) / (pow( 1 + ((alpha/local_size) *sum),beta));
                                sum = 0;
                                Layer_OutNeurons_GPU[output*fr*fc + row*fc + col] = value;
			}
		}

	}
#ifdef NORM_LAYER
	for(int N = 0; N < out; N++)
	{

		for(int W = 0; W < fr; W++)
		{
			for(int H = 0; H < fc; H++)
			{
                                printf("%f\n",Layer_OutNeurons_GPU[N*fr*fc + W*fc + H]);;
			}
		}
	}
#endif
}
void executeFCLayer(float *bias,float *Layer_InNeurons_GPU,float *Layer_Weights_GPU,float *Layer_OutNeurons_GPU,int output, int input,bool reLU,bool dropout)
{
        printf("Execute FC Layer of output : %d input %d\n",output,input);
        float product = 0.0,max = 0.0; int weight = 0,index = 0;
        int out = 0,in = 0;
	for(out=0; out < output ; out++)
	{
		for(in = 0; in < input; in++)
		{
                     product += Layer_InNeurons_GPU[in] * Layer_Weights_GPU[weight++];
		}
                product += bias[out];
		if(reLU == true)
		{
			if(product < 0) /* ReLU Layer */
				product = 0;
		}
		else
		{
			if(max < product)
			{
                                index = out;
				max = product;
			}
		}
                if(dropout == true)
		{

		}
		Layer_OutNeurons_GPU[out] = product;
#ifdef FC_DEBUG
		printf("%f\n",product);
#endif
		product = 0.0;
	}
        printf(" MAX from FC layer = %d\n",index);
}
void extract_weights(char *pFileName,float *layer_weights,bool bias)
{
	FILE * pFile1 = fopen (pFileName,"rb");
	char delim[2];
	if(bias == true)
		delim[0] = ' ';
	else
		delim[0] = '\n';
	delim[1] = 0;
	char *token;
	int count = 0;
	char *line = NULL;
	size_t len = 0;
	if (!(pFile1 != NULL))
            printf("File Not Found\n");
	if (pFile1 != NULL && (bias == false))
	{
		printf(" File FOUND %s\n",pFileName);
		{
			
			//fread(weights,sizeof(weights),1,pFile1);
			//token = strtok(weights,delim);
			//while(token != NULL)
			while (getline(&line, &len, pFile1) != -1)
			{
				token = strtok(line,delim);
				float temp_num = atof(token);
			        layer_weights[count] = temp_num;	
				//printf("%.8f\t",temp_num); 
				count++; 
			//	token = strtok(NULL,delim);
			}
		}
		printf("Final Count : %d\n",count);
		fclose(pFile1);
	}
	if (pFile1 != NULL && (bias == true))
	{
		printf(" File FOUND %s\n",pFileName);
		{

			char weights[94590] = "";
			fread(weights,sizeof(weights),1,pFile1);
			token = strtok(weights,delim);
			while(token != NULL)
			{
				float temp_num = atof(token);
			        layer_weights[count] = temp_num;	
				//printf("%.8f\t",temp_num); 
				count++; 
				token = strtok(NULL,delim);
			}
		}
		printf("Final Count : %d\n",count);
		fclose(pFile1);
	}

}

void Fill_weights(float *Layer1_Weights_CPU,float *Layer2_Weights_CPU,float *Layer3_Weights_CPU,float *Layer4_Weights_CPU,float *Layer5_Weights_CPU,float *Layer6_Weights_CPU,float *Layer7_Weights_CPU,float *Layer8_Weights_CPU)
{
	extract_weights("data/conv1.txt",Layer1_Weights_CPU,false);
	extract_weights("data/conv2.txt",Layer2_Weights_CPU,false);
	extract_weights("data/conv3.txt",Layer3_Weights_CPU,false);
	extract_weights("data/conv4.txt",Layer4_Weights_CPU,false);
	extract_weights("data/conv5.txt",Layer5_Weights_CPU,false);
	extract_weights("data/fc6.txt",Layer6_Weights_CPU,false);
	extract_weights("data/fc7.txt",Layer7_Weights_CPU,false);
	extract_weights("data/fc8.txt",Layer8_Weights_CPU,false);
	printf("Extracted Weights and Bias successfully\n");
}
void Fill_bias(float *bias_1,float *bias_2,float *bias_3,float *bias_4,float *bias_5,float *bias_6,float *bias_7,float *bias_8)
{
	extract_weights("data/bias1.txt",bias_1,true);
	extract_weights("data/bias2.txt",bias_2,true);
	extract_weights("data/bias3.txt",bias_3,true);
	extract_weights("data/bias4.txt",bias_4,true);
	extract_weights("data/bias5.txt",bias_5,true);
	extract_weights("data/bias6.txt",bias_6,true);
	extract_weights("data/bias7.txt",bias_7,true);
	extract_weights("data/bias8.txt",bias_8,true);
}
void readIn(float *layer1)
{
	FILE *fp = fopen ("data/input.txt","rb");
	size_t len;
        char delim[1];
        delim[0] = '\n';
	int count = 0;
	char *token;
        char *line = NULL;
	if (fp != NULL)
	{
		printf(" File FOUND\n");
		{
			while ((getline(&line, &len, fp)) != -1)
			{
                                token = strtok(line,delim);
                                layer1[count] = atof(token);
				count++;		
			}
			printf("READ INPUT Final Count :: %d\n",count);		
		}
		fclose(fp);
	}
	else
	{
		printf(" File NOt FOUND\n");
	}
}

void NeuralNetwork()
{
        /* Read Input File 227*227*3 */	
	float *Layer1_Neurons_CPU = (float*) malloc (INPUT_SIZE * sizeof(float));
	readIn(Layer1_Neurons_CPU);

        /* Declaration of Bias and Weights for CPU */ 
	float bias_1[96],bias_2[256],bias_3[384],bias_4[384],bias_5[256],bias_6[4096],bias_7[4096],bias_8[1000];
	float *Layer1_Weights_CPU = (float *)malloc(sizeof(float) *(L1_KERNEL_SIZE * L1_OUT));
	float *Layer2_Weights_CPU = (float *)malloc(sizeof(float) *(L2_KERNEL_SIZE * L2_OUT));
	float *Layer3_Weights_CPU = (float *)malloc(sizeof(float) *(L3_KERNEL_SIZE * L3_OUT));
	float *Layer4_Weights_CPU = (float *)malloc(sizeof(float) *(L4_KERNEL_SIZE * L4_OUT));
	float *Layer5_Weights_CPU = (float *)malloc(sizeof(float) *(L5_KERNEL_SIZE * L5_OUT));
	float *Layer6_Weights_CPU = (float *)malloc(sizeof(float) *(4096*256*6*6));
	float *Layer7_Weights_CPU = (float *)malloc(sizeof(float) *(4096*4096));
	float *Layer8_Weights_CPU = (float *)malloc(sizeof(float) *(4096*1000));
        
	/* Fill Bias and Weights */	
	Fill_bias(bias_1,bias_2,bias_3,bias_4,bias_5,bias_6,bias_7,bias_8);
        Fill_weights(Layer1_Weights_CPU,Layer2_Weights_CPU,Layer3_Weights_CPU,Layer4_Weights_CPU,Layer5_Weights_CPU,Layer6_Weights_CPU,Layer7_Weights_CPU,Layer8_Weights_CPU);

        /* if CPU = 1 then CPU version of code ,else CUDA code */
	/* First Layer convolution + ReLU + pooling */ 
        float *Layer2_Neurons_CPU = (float *)malloc(sizeof(float) *(96*55*55));
	executeFirstLayer(bias_1,Layer1_Neurons_CPU,Layer1_Weights_CPU,Layer2_Neurons_CPU,4,227,55,55,96);
	/*Normalisation */	
        float *Layer2_Norm_CPU = (float *)malloc(sizeof(float) *(96*55*55));
        executelrnNorm(Layer2_Neurons_CPU,0.0001,0.75,5,96,55,55,Layer2_Norm_CPU);
         /* Max Pool */
        float *Layer2_pool_CPU = (float *)malloc(sizeof(float) *(96*27*27));
        pooling(Layer2_Norm_CPU,Layer2_pool_CPU,96,27,27,3,2,55,55);

	/* Second Layer */	
	float *Layer3_Neurons_CPU = (float *)malloc(sizeof(float) *(256*27*27));
	execute3Dconvolution(bias_2,Layer2_pool_CPU,Layer2_Weights_CPU,Layer3_Neurons_CPU,256,27,27,1,5,2,96,2);
        /*Normalisation */
        float *Layer3_Norm_CPU = (float *)malloc(sizeof(float) *(256*27*27));
        executelrnNorm(Layer3_Neurons_CPU,0.0001,0.75,5,256,27,27,Layer3_Norm_CPU);
        /* Max Pool */ 
	float *Layer3_pool_CPU = (float *)malloc(sizeof(float) *(256*13*13));
        pooling(Layer3_Norm_CPU,Layer3_pool_CPU,256,13,13,3,2,27,27);

	/* Third Layer convolution + ReLU  */ 
	float *Layer4_Neurons_CPU = (float *)malloc(sizeof(float) *(384*13*13));
	execute3Dconvolution(bias_3,Layer3_pool_CPU,Layer3_Weights_CPU,Layer4_Neurons_CPU,384,13,13,1,3,1,256,1);
	
	/* Fourth Layer convolution + ReLU  */ 
	float *Layer5_Neurons_CPU = (float *)malloc(sizeof(float) *(384*13*13));
	execute3Dconvolution(bias_4,Layer4_Neurons_CPU,Layer4_Weights_CPU,Layer5_Neurons_CPU,384,13,13,1,3,1,384,2);

	/* Fifth Layer convolution + ReLU + pooling */ 
	float *fc6_Neurons_CPU = (float *)malloc(sizeof(float) *(256*13*13));
	execute3Dconvolution(bias_5,Layer5_Neurons_CPU,Layer5_Weights_CPU,fc6_Neurons_CPU,256,13,13,1,3,1,384,2);
	float *fc6_pool_CPU = (float *)malloc(sizeof(float) *(256*6*6));
        pooling(fc6_Neurons_CPU,fc6_pool_CPU,256,6,6,3,2,13,13);

        /* Sixth Layer Fully connected + ReLU */	
	float *fc7_Neurons_CPU = (float *)malloc(sizeof(float) * (4096));
	executeFCLayer(bias_6,fc6_pool_CPU,Layer6_Weights_CPU,fc7_Neurons_CPU,4096,(256*6*6),true,true);

	/* Seventh Layer Fully connected + ReLU */	
	float *fc8_Neurons_CPU = (float *)malloc(sizeof(float) * (4096));
	executeFCLayer(bias_7,fc7_Neurons_CPU,Layer7_Weights_CPU,fc8_Neurons_CPU,4096,4096,true,true);

        /*Eigth Layer */
	float *fc9_Neurons_CPU = (float *)malloc(sizeof(float) * (1000));
	executeFCLayer(bias_8,fc8_Neurons_CPU,Layer8_Weights_CPU,fc9_Neurons_CPU,1000,4096,false,false);
}



int main()
{
	NeuralNetwork();
}
