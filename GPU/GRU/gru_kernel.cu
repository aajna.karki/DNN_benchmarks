
#include<stdio.h>
#include<math.h>

float sigmoid(float x)
{
    return 0.5*tanh(0.5*x) + 0.5;
}
__device__ float hard_sigmoid(float x)
{
    if (x<-2.5)
        return 0;
    else
        if(x>2.5)
            return 1;
        else
            return(0.2*x + 0.5);
}

__device__ float gpu_sigmoid(float x)
{
    return 0.5*tanh(0.5*x) + 0.5;
}

__global__ void GPU_forward_pass_gru(float *x_t, float *GPU_states ,float *GPU_kernel_h_w,float *GPU_kernel_r_w,float *GPU_kernel_z_w,float *GPU_bias_h,float *GPU_bias_r,float *GPU_bias_z,float *GPU_rec_h_w,float *GPU_rec_r_w,float *GPU_rec_z_w)
{
        int index = threadIdx.x * blockDim.x + threadIdx.y;
        float z,h;
        // Number of units = 100, hardcoded to 100
        int num_units = 100;
        __shared__  float update_gate[100];
        
	float rec_z = 0,rec_r = 0,rec_h = 0;
        for(int i=0;i < num_units; i++)
	{
             rec_z += GPU_states[i] * GPU_rec_z_w[index + num_units * i];
             rec_r += GPU_states[i] * GPU_rec_r_w[index + num_units * i];
              
	}
	__syncthreads();
	z = hard_sigmoid((x_t[0] * GPU_kernel_z_w[index]) +  rec_z + GPU_bias_z[index]);
	update_gate[index] = hard_sigmoid((x_t[0] * GPU_kernel_r_w[index]) +  rec_r + GPU_bias_r[index]);

        for(int i=0;i < num_units; i++)
	{
             rec_h += update_gate[i] * GPU_states[i] * GPU_rec_h_w[index + num_units * i];
	}
	__syncthreads();

	h = tanh((x_t[0] * GPU_kernel_h_w[index]) +  rec_h + GPU_bias_h[index]);
	GPU_states[index] = z * GPU_states[index] + ( 1 - z) * h;
}
