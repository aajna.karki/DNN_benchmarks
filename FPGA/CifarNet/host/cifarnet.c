/***************************************************************
 *   CIFARNET HOST CODE IMPLEMENTATION
 *   Author: Spoorthi Mysore Shivakumar
 * 	 Masters Project 295-B Spring 2018
***************************************************************/

/* Header files */
#include <stdlib.h>
#include "platform.h"
#include "xil_mmu.h"
#include "xil_cache.h"
#include "xil_cache_l.h"
#include <string.h>
#include <stdio.h>
#include "xparameters.h"
#include "xtime_l.h"
#include "cifarnet.h"

void print(char *str);

XTime tStart, tEnd;

/* Converting input image data into R,G,B channels */
void ConvertInput(int *Data_Layer_CPU_R, int *Data_Layer_CPU_G, int *Data_Layer_CPU_B, int *Data_Layer_CPU)
{
	for(int i=0; i<32*32*3; i+=3)
	{
		Data_Layer_CPU_R[i/3] = Data_Layer_CPU[i];
		Data_Layer_CPU_G[i/3] = Data_Layer_CPU[i+1];
		Data_Layer_CPU_B[i/3] = Data_Layer_CPU[i+2];
	}
}





/* Base address of registers for FPGA */

/* First Layer */
volatile char *control = (volatile char*)0x43C00000;
volatile int *wg_x = (volatile int*)0x43C00010;
volatile int *wg_y = (volatile int*)0x43C00018;
volatile int *wg_z = (volatile int*)0x43C00020;
volatile int *o_x = (volatile int*)0x43C00028;
volatile int *o_y = (volatile int*)0x43C00030;
volatile int *o_z = (volatile int*)0x43C00038;
volatile int *Layer1_Weights_hw = (volatile int*)0x43C00040;
volatile int *Data_R_hw = (volatile int*)0x43C00048;
volatile int *Data_G_hw = (volatile int*)0x43C00050;
volatile int *Data_B_hw = (volatile int*)0x43C00058;
volatile int *Layer1_Features_hw = (volatile int*)0x43C00060;

/* Pool 1 */
volatile char *control_p = (volatile char*)0x43C10000;
volatile int *wg_x_p = (volatile int*)0x43C10010;
volatile int *wg_y_p = (volatile int*)0x43C10018;
volatile int *wg_z_p = (volatile int*)0x43C10020;
volatile int *o_x_p = (volatile int*)0x43C10028;
volatile int *o_y_p = (volatile int*)0x43C10030;
volatile int *o_z_p = (volatile int*)0x43C10038;
volatile int *Layer1_Features_hw_p = (volatile int*)0x43C10040;
volatile int *Layer1_Pool_hw_p = (volatile int*)0x43C10048;

/* Second Layer */
volatile char *control_2 = (volatile char*)0x43C20000;
volatile int *wg_x_2 = (volatile int*)0x43C20010;
volatile int *wg_y_2 = (volatile int*)0x43C20018;
volatile int *wg_z_2 = (volatile int*)0x43C20020;
volatile int *o_x_2 = (volatile int*)0x43C20028;
volatile int *o_y_2 = (volatile int*)0x43C20030;
volatile int *o_z_2 = (volatile int*)0x43C20038;
volatile int *Layer2_Weights_hw_2 = (volatile int*)0x43C20040;
volatile int *Layer1_Pool_hw_2 = (volatile int*)0x43C20048;
volatile int *Layer2_Features_hw_2 = (volatile int*)0x43C20050;

/* Pool 2 */
volatile char *control_p2 = (volatile char*)0x43C30000;
volatile int *wg_x_p2 = (volatile int*)0x43C30010;
volatile int *wg_y_p2 = (volatile int*)0x43C30018;
volatile int *wg_z_p2 = (volatile int*)0x43C30020;
volatile int *o_x_p2 = (volatile int*)0x43C30028;
volatile int *o_y_p2 = (volatile int*)0x43C30030;
volatile int *o_z_p2 = (volatile int*)0x43C30038;
volatile int *Layer2_Weights_hw_p2 = (volatile int*)0x43C30040;
volatile int *Layer2_Pool_hw_p2 = (volatile int*)0x43C30048;
volatile int *Layer2_Features_hw_p2 = (volatile int*)0x43C30050;

/* Third Layer */
volatile char *control_3 = (volatile char*)0x43C40000;
volatile int *wg_x_3 = (volatile int*)0x43C40010;
volatile int *wg_y_3 = (volatile int*)0x43C40018;
volatile int *wg_z_3 = (volatile int*)0x43C40020;
volatile int *o_x_3 = (volatile int*)0x43C40028;
volatile int *o_y_3 = (volatile int*)0x43C40030;
volatile int *o_z_3 = (volatile int*)0x43C40038;
volatile int *Layer3_Weights_hw_3 = (volatile int*)0x43C40040;
volatile int *Layer2_Pool_hw_3 = (volatile int*)0x43C40048;
volatile int *Layer3_Features_hw_3 = (volatile int*)0x43C40050;

/* Pool 3  */
volatile char *control_p3 = (volatile char*)0x43C50000;
volatile int *wg_x_p3 = (volatile int*)0x43C50010;
volatile int *wg_y_p3 = (volatile int*)0x43C50018;
volatile int *wg_z_p3 = (volatile int*)0x43C50020;
volatile int *o_x_p3 = (volatile int*)0x43C50028;
volatile int *o_y_p3 = (volatile int*)0x43C50030;
volatile int *o_z_p3 = (volatile int*)0x43C50038;
volatile int *Layer3_Pool_hw_p3 = (volatile int*)0x43C50048;
volatile int *Layer3_Features_hw_p3 = (volatile int*)0x43C50040;

/* Fourth Layer */
volatile char *control_4 = (volatile char*)0x43C60000;
volatile int *wg_x_4 = (volatile int*)0x43C60010;
volatile int *wg_y_4 = (volatile int*)0x43C60018;
volatile int *wg_z_4 = (volatile int*)0x43C60020;
volatile int *o_x_4 = (volatile int*)0x43C60028;
volatile int *o_y_4 = (volatile int*)0x43C60030;
volatile int *o_z_4 = (volatile int*)0x43C60038;
volatile int *Layer4_Weights_hw_4 = (volatile int*)0x43C60040;
volatile int *Layer3_Pool_hw_4 = (volatile int*)0x43C60048;
volatile int *Layer4_Features_hw_4 = (volatile int*)0x43C60050;

/* Fifth Layer */
volatile char *control_5 = (volatile char*)0x43C70000;
volatile int *wg_x_5 = (volatile int*)0x43C70010;
volatile int *wg_y_5 = (volatile int*)0x43C70018;
volatile int *wg_z_5 = (volatile int*)0x43C70020;
volatile int *o_x_5 = (volatile int*)0x43C70028;
volatile int *o_y_5 = (volatile int*)0x43C70030;
volatile int *o_z_5 = (volatile int*)0x43C70038;
volatile int *Layer5_Weights_hw_5 = (volatile int*)0x43C70040;
volatile int *Layer5_Features_hw_5 = (volatile int*)0x43C70048;
volatile int *Layer4_Features_hw_5 = (volatile int*)0x43C70050;

/* Main function */
int main()
{

	/* Initialization */
	init_platform();
	Xil_SetTlbAttributes(0x43c00000,0x10c06); /* non cacheable */

	/* Host parameter declaration for each layer */
	int *Data_Layer_CPU;
	int *Data_R_host;
	int *Data_G_host;
	int *Data_B_host;

	float *Layer1_Weights_host;
	float *Layer2_Weights_host;
	float *Layer3_Weights_host;
	float *Layer4_Weights_host;
	float *Layer5_Weights_host;

	float* Layer1_Features_host;
	float* Layer2_Features_host;
	float* Layer3_Features_host;
	float* Layer4_Features_host;
	float* Layer5_Features_host;

	float* Layer1_Pool_host;
	float* Layer2_Pool_host;
	float* Layer3_Pool_host;


	/* Allocating host memory */
	Data_R_host = (int*) malloc (32*32*sizeof(int));
	Data_G_host = (int*) malloc (32*32*sizeof(int));
	Data_B_host = (int*) malloc (32*32*sizeof(int));
	Data_Layer_CPU = (int*) malloc (3*32*32*sizeof(int));

	Layer1_Weights_host = (float*) malloc (3*32*32 * sizeof(float));
	Layer2_Weights_host = (float*) malloc (5*5*32*32 * sizeof(float));
	Layer3_Weights_host = (float*) malloc (5*5*32*64 * sizeof(float));
	Layer4_Weights_host = (float*) malloc (64*4*4*64 * sizeof(float));
	Layer5_Weights_host = (float*) malloc (64*9 * sizeof(float));

	Layer1_Features_host = (float*) malloc (32*32*32 * sizeof(float));
	Layer2_Features_host = (float*) malloc (32*16*16 * sizeof(float));
	Layer3_Features_host = (float*) malloc (64*8*8 * sizeof(float));
	Layer4_Features_host = (float*) malloc (64 * sizeof(float));
	Layer5_Features_host = (float*) malloc (9 * sizeof(float));

	Layer1_Pool_host = (float*) malloc (32*16*16*sizeof(float));
	Layer2_Pool_host = (float*) malloc (32*8*8*sizeof(float));
	Layer3_Pool_host = (float*) malloc (64*4*4*sizeof(float));

	/* Copying data and matrix arrays */
	for(int i=0; i<3072; i++)
	{
		Data_Layer_CPU[i] = Data_Layer_CPU_copy[i];
	}

	ConvertInput(Data_R_host, Data_G_host, Data_B_host, Data_Layer_CPU);


	for(int i=0; i<2400; i++)
	{
		Layer1_Weights_host[i] = Layer1_Weights_copy[i];
	}

	for(int i=0; i<25600; i++)
	{
		Layer2_Weights_host[i] = Layer2_Weights_copy[i];
	}

	for(int i=0; i<51200; i++)
	{
		Layer3_Weights_host[i] = Layer3_Weights_copy[i];
	}

	for(int i=0; i<65536; i++)
	{
		Layer4_Weights_host[i] = Layer4_Weights_copy[i];
	}

	for(int i=0; i<576; i++)
	{
		Layer5_Weights_host[i] = Layer5_Weights_copy[i];
	}



	Xil_DCacheFlush();

	/* Program registers of the layers */
	/* Mapping hardware parameters to corresponding host parameters */
	*Data_R_hw = (int)Data_R_host;
	*Data_G_hw = (int)Data_G_host;
	*Data_B_hw = (int)Data_B_host;

	*Layer1_Weights_hw = (int)Layer1_Weights_host;
	*Layer1_Features_hw = (int)Layer1_Features_host;

	*Layer1_Pool_hw_p = (int)Layer1_Pool_host;
	*Layer1_Features_hw_p = (int)Layer1_Features_host;

	*Layer1_Pool_hw_2 = (int)Layer1_Pool_host;
	*Layer2_Weights_hw_2 = (int)Layer2_Weights_host;
	*Layer2_Features_hw_2 = (int)Layer2_Features_host;

	*Layer2_Pool_hw_p2 = (int)Layer2_Pool_host;
	*Layer2_Features_hw_p2 = (int)Layer2_Features_host;
	*Layer2_Weights_hw_p2 = (int)Layer2_Features_host;

	*Layer2_Pool_hw_3 = (int)Layer2_Pool_host;
	*Layer3_Weights_hw_3 = (int)Layer3_Weights_host;
	*Layer3_Features_hw_3 = (int)Layer3_Features_host;

	*Layer3_Pool_hw_p3 = (int)Layer3_Pool_host;
	*Layer3_Features_hw_p3 = (int)Layer3_Features_host;

	*Layer3_Pool_hw_4 = (int)Layer3_Pool_host;
	*Layer4_Weights_hw_4 = (int)Layer4_Weights_host;
	*Layer4_Features_hw_4 = (int)Layer4_Features_host;

	*Layer5_Weights_hw_5 = (int)Layer5_Weights_host;
	*Layer4_Features_hw_5 = (int)Layer4_Features_host;
	*Layer5_Features_hw_5 = (int)Layer5_Features_host;

	/* Control registers */
	unsigned int con = *control;
	/* set the workgroup identity */
	*wg_y = 0;
	*wg_z = 0;
	*wg_x = 0;
	*o_x = 0;
	*o_y = 0;
	*o_z = 0;

	*wg_y_p = 0;
	*wg_z_p = 0;
	*wg_x_p = 0;
	*o_x_p = 0;
	*o_y_p = 0;
	*o_z_p = 0;


	*wg_y_2 = 0;
	*wg_z_2 = 0;
	*wg_x_2 = 0;
	*o_x_2 = 0;
	*o_y_2 = 0;
	*o_z_2 = 0;

	*wg_y_p2 = 0;
	*wg_z_p2 = 0;
	*wg_x_p2 = 0;
	*o_x_p2 = 0;
	*o_y_p2 = 0;
	*o_z_p2 = 0;

	*wg_y_3 = 0;
	*wg_z_3 = 0;
	*wg_x_3 = 0;
	*o_x_3 = 0;
	*o_y_3 = 0;
	*o_z_3 = 0;

	*wg_y_p3 = 0;
	*wg_z_p3 = 0;
	*wg_x_p3 = 0;
	*o_x_p3 = 0;
	*o_y_p3 = 0;
	*o_z_p3 = 0;

	*wg_y_4 = 0;
	*wg_z_4 = 0;
	*wg_x_4 = 0;
	*o_x_4 = 0;
	*o_y_4 = 0;
	*o_z_4 = 0;

	*wg_y_5 = 0;
	*wg_z_5 = 0;
	*wg_x_5 = 0;
	*o_x_5 = 0;
	*o_y_5 = 0;
	*o_z_5 = 0;


	/* Control signals for FPGA to start operation and check completion */

	/* Start timer */
	XTime_GetTime(&tStart);

	/* Layer 1 */
	*control = *control | 1; /* start */
	while (! ((*control) & 2));

	/* Pool 1 */
	con = *control_p;
	print("\n\r");
	print("\nStarting OpenCL kernel execution\n\r");

	*control_p = *control_p | 1; /* start */
	/* waiting for hardware to report "done" */
	while (! ((*control_p) & 2));

	/* Layer 2 */
	con = *control_2;
	*control_2 = *control_2 | 1; /* start */
	/* waiting for hardware to report "done" */
	while (! ((*control_2) & 2));

	/* Pool 2 */
	con = *control_p2;
	*control_p2 = *control_p2 | 1; /* start */
	/* waiting for hardware to report "done" */
	while (! ((*control_p2) & 2));

	/* Layer 3 */
	con = *control_3;
	*control_3 = *control_3 | 1; /* start */
	/* waiting for hardware to report "done" */
	while (! ((*control_3) & 2));

	/* Pool 3 */
	con = *control_p3;
	*control_p3 = *control_p3 | 1; /* start */
	/* waiting for hardware to report "done" */
	while (! ((*control_p3) & 2));

	/* Layer 4 */
	con = *control_4;
	*control_4 = *control_4 | 1; /* start */
	/* waiting for hardware to report "done" */
	while (! ((*control_4) & 2));

	/* Layer 5 */
	con = *control_5;	*control_5 = *control_5 | 1; /* start */
	/* waiting for hardware to report "done" */
	while (! ((*control_5) & 2));

	/* End timer */
	XTime_GetTime(&tEnd);


	printf("\n Final computation for each layer");
	for (int i = 0; i < 9; i++)
	{
		printf("%d \n %f",i, Layer5_Features_host[i]);
	}

	int index = 0;
	float max = 0.0;
	for(int i=0; i<9; i++)
	{
		if(Layer5_Features_host[i] > max)
		{
			max = Layer5_Features_host[i];
			index = i;
		}
	}

	printf("\n\n PREDICTED CLASS: %d",index);
	printf("\n\n PREDICTED CLASS: %d",index);
	Xil_DCacheInvalidate();


	//print("\n Result validated\n");
	cleanup_platform();
	return 0;
}
