__kernel void __attribute__ ((reqd_work_group_size(111,1,1))) pooling1( __global float *Layer2_Neurons_GPU,__global float *Layer2_pool_GPU)
{
    int row = get_local_id(0);
    int col = get_group_id(0);
    float max = 0;
        for(int output =0;output < 96 ;output++)
        {
            if(row%2 != 0)
            {
                if(col%2 != 0)
                {
                    for(int i = row-1; i <= row+1; i++)
                    {
			         if(i>110)
				     {
				      break;
				     }
                     for(int j = col-1; j <= col+1; j++)
                        {
			            if(j>110)
			    	    {
			    	     break;
			    	     }
                            if(max < ((Layer2_Neurons_GPU[output*111*111+i*111+j])))
                            {
                                max =((Layer2_Neurons_GPU[output*111*111+i*111+j]));
                            }

                        }
                    }
                    Layer2_pool_GPU[output*55*55+((row-1)/2)*55+(col-1)/2] = max;
                    max = 0.0;
                }
            }
        }
}


